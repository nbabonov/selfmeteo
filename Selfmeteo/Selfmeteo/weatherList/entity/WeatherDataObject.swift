//
//  WeatherDataObject.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 01/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

class WeatherDataObject: Codable {
    
    //MARK: Properties
    static let UDKey = "CurrentWeatherData"
    static let lastSavedDegreeUDKey = "LastSavedDegree"
    
    var current: Weather
    var forecast: WeatherForecast
    
    enum CodingKeys: String, CodingKey {
        case current
        case forecast
    }
    
    init(current: Weather, forecast: WeatherForecast) {
        self.current = current
        self.forecast = forecast
    }
    
    // MARK: Codable
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        current = try container.decode(Weather.self, forKey: .current)
        forecast = try container.decode(WeatherForecast.self, forKey: .forecast)
    }
    
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(current, forKey: .current)
        try container.encode(forecast, forKey: .forecast)
    }
    
    // MARK: User Defaults
    func saveToUD(needUpdateSavedDegree: Bool = true) {
        guard let encoded = try? JSONEncoder().encode(self) else {return}
        UserDefaults.standard.set(encoded, forKey: WeatherDataObject.UDKey)
        if needUpdateSavedDegree == true {
            UserDefaults.standard.set(current.details.temp, forKey: WeatherDataObject.lastSavedDegreeUDKey)
        }
    }
    
    static func getFromUD() -> WeatherDataObject? {
        guard let data = UserDefaults.standard.object(forKey: WeatherDataObject.UDKey) as? Data else {return nil}
        return try? JSONDecoder().decode(WeatherDataObject.self, from: data)
    }
    
    static func savedDegree() -> Float? {
        return UserDefaults.standard.object(forKey: WeatherDataObject.lastSavedDegreeUDKey) as? Float
    }
    
}
