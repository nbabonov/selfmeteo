//
//  WeatherPresenter.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation

class WeatherPresenter: NSObject, WeatherViewOutput {
    
    // MARK: Properties
    weak var view: WeatherViewInput?
    var router: WeatherRouterInput?
    var interactor: WeatherInteractorInput?
    
    var weatherData: WeatherDataObject? = WeatherDataObject.getFromUD() {
        didSet {
            view?.reloadData()
        }
    }
    
    // MARK: Life cycle
    func viewIsLoaded() {
        configureObservers()
        configureLocationManager()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Configuring
    func configureLocationManager() {
        ObserveLocationManager.shared.delegate = self
    }
    
    func configureObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(saveCurrentWeather), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    // MARK: Data
    func startLoading() {
        interactor?.loadWeatherData(completion: {[weak self] (_, weatherData, error) in
            if let error = error {
                self?.router?.show(error)
                return
            }
            
            self?.weatherData = weatherData
        })
    }
    
    // MARK: Observers
    
    @objc
    func saveCurrentWeather() {
        interactor?.saveCurrent(weatherData)
    }
    
}

extension WeatherPresenter: GeolocationChangesProtocol {
    func authorizationDenied() {
        startLoading()
        router?.showDenyLocationAlert()
    }
    
    func coordinateChanged() {
        startLoading()
    }
    
}
