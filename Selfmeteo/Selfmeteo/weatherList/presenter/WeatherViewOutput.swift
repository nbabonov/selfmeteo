//
//  WeatherViewOutput.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

protocol WeatherViewOutput {
    var weatherData: WeatherDataObject? {get}
    func viewIsLoaded()
}
