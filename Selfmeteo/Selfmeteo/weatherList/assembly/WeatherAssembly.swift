//
//  WeatherAssembly.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct WeatherAssembly {
    
    static func createWeatherModule() -> WeatherVC? {
        guard let view = Storyboards.weather.instantiateViewController(withIdentifier: WeatherVC.storyboardID) as? WeatherVC else {return nil}
        
        let router = WeatherRouter()
        let presenter = WeatherPresenter()
        let interactor = WeatherInteractor()
        
        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        return view
    }
    
}
