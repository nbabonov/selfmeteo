//
//  WeatherSectionHeader.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 01/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit

class WeatherSectionHeader: UIView {
    
    // MARK: Properties
    static let nibName = "WeatherSectionHeader"
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var separatorHeights: [NSLayoutConstraint]!
    
    var forecast: [Weather] = []
    let sectionInset = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13)
    
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        configureCollectionView()
    }
    
    // MARK: Configuring
    func configureUI() {
        separatorHeights.forEach({$0.constant = 1/UIScreen.main.scale})
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: WeatherForecastPage.reuseID, bundle: nil), forCellWithReuseIdentifier: WeatherForecastPage.reuseID)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

}

extension WeatherSectionHeader: UICollectionViewDelegate { }

extension WeatherSectionHeader: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WeatherForecastPage.reuseID, for: indexPath) as? WeatherForecastPage else {return UICollectionViewCell()}
        cell.weather = forecast[indexPath.row]
        return cell
    }
    
}

extension WeatherSectionHeader: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return WeatherForecastPage.pageSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }
    
}
