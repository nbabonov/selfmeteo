//
//  WeatherVC.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import Foundation

class WeatherVC: BaseViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    static let storyboardID = "WeatherVCID"
    var output: WeatherViewOutput?
    var header = Bundle.main.loadNibNamed(WeatherTableHeader.nibName, owner: self, options: nil)?.first as? WeatherTableHeader
    
    // MARK: Life cycle
    override func viewDidLoad() {
        gradientColors = (.darkMauve, .lightMauve)
        super.viewDidLoad()
        configureTableView()
        output?.viewIsLoaded()
    }
    
    // MARK: Configuring
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = header
        tableView.tableFooterView = UIView(frame: .zero)
        header?.frame = CGRect(x: 0, y: 0, width: ScreenDefines.width, height: WeatherTableHeader.height)
        header?.weather = output?.weatherData?.current
    }
    
}

extension WeatherVC: WeatherViewInput {
    func reloadData() {
        header?.weather = output?.weatherData?.current
        tableView.reloadData()
    }
}

extension WeatherVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.weatherData?.forecast.days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherDayCell.reuseID, for: indexPath) as? WeatherDayCell else {return UITableViewCell()}
        cell.weather = output?.weatherData?.forecast.days[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = Bundle.main.loadNibNamed(WeatherSectionHeader.nibName, owner: self, options: nil)?.first as? WeatherSectionHeader
        header?.forecast = output?.weatherData?.forecast.list ?? []
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return WeatherForecastPage.pageSize.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return WeatherDayCell.height
    }
}

extension WeatherVC: UITableViewDelegate { }
