//
//  WeatherForecastPage.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 01/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import SDWebImage

class WeatherForecastPage: UICollectionViewCell {
    
    // MARK: Properties
    static let reuseID = "WeatherForecastPage"
    static let pageSize = CGSize(width: 50, height: 115)

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    // MARK: Life cycle
    var weather: Weather? {
        didSet {
            guard let weather = weather else {return}
            timeLabel.text = timeFor(weather.date())
            tempLabel.text = String(Int(weather.details.temp)) + "°"
            if let url = weather.type.first?.iconURL() {
                weatherImageView.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        weatherImageView.image = nil
    }
    
    // MARK: Helpers
    private func timeFor(_ date: Date) -> String {
        let df = DateFormatter()
        df.dateFormat = "H"
        return df.string(from: date)
    }

}
