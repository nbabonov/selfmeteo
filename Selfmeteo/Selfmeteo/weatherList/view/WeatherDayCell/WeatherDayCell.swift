//
//  WeatherDayCell.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 02/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit

class WeatherDayCell: UITableViewCell {
    
    // MARK: Properties
    static let height: CGFloat = 35
    static let reuseID = "WeatherDayCellID"

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    // MARK: Life cycle
    var weather: (date: Date, type: [WeatherType]?, minTemp: Int?, maxTemp: Int?)? {
        didSet {
            guard let weather = weather else {return}
            configureDateLabel(date: weather.date)
            
            minTempLabel.text = String(weather.minTemp ?? 0)
            maxTempLabel.text = String(weather.maxTemp ?? 0)
            
            if let url = weather.type?.first?.iconURL() {
                weatherImageView.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        weatherImageView.image = nil
    }
    
    // MARK: Configuring
    private func configureDateLabel(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        dayLabel.text = dateFormatter.string(from: date).capitalizingFirstLetter()
    }

}
