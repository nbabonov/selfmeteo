//
//  WeatherTableHeader.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit

class WeatherTableHeader: UITableViewHeaderFooterView {
    
    // MARK: Properties
    static let height: CGFloat = 330
    static let nibName = "WeatherTableHeader"
    private static let degreeFont = UIFont.SFProText.light(95)

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    
    //MARK: Life cycle
    var weather: Weather? {
        didSet {
            guard let weather = weather else {return}
            cityLabel.text = weather.city?.name
            infoLabel.text = weather.type.first?.info.capitalizingFirstLetter()
            degreeLabel.text = String(Int(weather.details.temp)) + "°"
            degreeLabel.font = WeatherTableHeader.degreeFont
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            dayLabel.text = dateFormatter.string(from: weather.date()).capitalizingFirstLetter()
            maxTempLabel.text = String(Int(weather.details.tempMax))
            minTempLabel.text = String(Int(weather.details.tempMin))
        }
    }

}
