//
//  WeatherRouter.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import Foundation

class WeatherRouter: WeatherRouterInput {
    
    func show(_ error: NSError?) {
        let action = UIAlertAction(title: "ОK", style: UIAlertAction.Style.cancel, handler: nil)
        
        let alertController = UIAlertController(title: "Внимание!", message: error?.localizedDescription ?? "Неизвестная ошибка, попробуйте позже", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(action)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func showDenyLocationAlert() {
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Настройки", style: .default) { _ in
            guard let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) else {return}
            UIApplication.shared.openURL(url)
        }
        
        let alertController = UIAlertController(title: "Внимание!", message: Errors.geolocationDeny.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(settingsAction)
        alertController.addAction(cancel)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}
