//
//  WeatherInteractor.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

class WeatherInteractor: WeatherInteractorInput {
    func saveCurrent(_ weather: WeatherDataObject?) {
        guard let weather = weather else {return}
        weather.saveToUD()
    }
    
    func loadWeatherData(completion: @escaping (Bool, WeatherDataObject?, NSError?) -> Void) {
        WeatherDataManager.updateWeatherData(coordinate: ObserveLocationManager.shared.currentCoordinate, completion: completion)
    }
    
}
