//
//  AppDelegate.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // MARK: App Life Cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureRootVC()
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        ObserveLocationManager.shared.startForegroundUpdating()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        ObserveLocationManager.shared.stopForegroundUpdating()
    }
    
    // MARK: Configuring
    private func configureRootVC() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = WeatherAssembly.createWeatherModule()
        window?.makeKeyAndVisible()
    }

}

