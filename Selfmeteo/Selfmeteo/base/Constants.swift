//
//  Constants.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation

struct API {
    private static let isProduction = true
    static let serverURL = isProduction ? Server.prod.rawValue : Server.test.rawValue
    
    enum Server: String {
        case test = "http://test.api.openweathermap.org/data/"
        case prod = "http://api.openweathermap.org/data/"
        case images = "http://openweathermap.org/img/wn/"
    }
    
    struct Paths {
        static let current = "2.5/weather"
        static let forecast = "2.5/forecast"
    }
    
    struct Keys {
        static let openWeatherMap = "4f2b47667f999feeb51896489fc9431d"
    }
    
}

public struct Defaults {
    static let metric = "metric"
    static let localization = "ru"
    static let absDegreeToNotify: Float = 3
    static let location = CLLocationCoordinate2D.init(latitude: 56.872502, longitude: 60.587145) // Екатеринбург
}

public struct Errors {
    static let noInternet = NSError(domain: "selfmeteo", code: -20000, userInfo: [NSLocalizedDescriptionKey: "Отсутствует соединение с Интернетом"])
    static let parseFailure = NSError(domain: "selfmeteo", code: -20001, userInfo: [NSLocalizedDescriptionKey: "Упс! Что-то пошло не так"])
    static let geolocationDeny = NSError(domain: "selfmeteo", code: -20002, userInfo: [NSLocalizedDescriptionKey: "Для корректного отображения погоды в вашем городе разрешите использование геолокационных данных в настройках приложения"])
    
}

struct Storyboards {
    static let weather = UIStoryboard(name: "weather", bundle: nil)
}

public struct ScreenDefines {
    static let width = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
    static let scale = UIScreen.main.scale
}
