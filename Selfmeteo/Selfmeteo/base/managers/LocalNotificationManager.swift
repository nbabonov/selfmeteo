//
//  LocalNotificationManager.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 02/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import Foundation

class LocalNotificationManager {
    
    static func registerForNotifications() {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings.init(types: [.alert, .badge, .sound], categories: nil))
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    static func showNotificationWith(_ data: WeatherDataObject) {
        let notification = UILocalNotification()
        let degree = Int(data.current.details.temp)
        
        notification.alertBody = "В настоящий момент на улице \(degree) \(String.getStringWithEnding(maleBasis: "градус", count: degree)) 😊"
        notification.soundName = UILocalNotificationDefaultSoundName
        
        UIApplication.shared.presentLocalNotificationNow(notification)
    }
    
}
