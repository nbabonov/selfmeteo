//
//  WeatherDataManager.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 01/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import CoreLocation
import Foundation

typealias WeatherCompletion = (_ success: Bool, _ dataObject: WeatherDataObject?, _ error: NSError?) -> Void

class WeatherDataManager {

    static func updateWeatherData(coordinate: CLLocationCoordinate2D, completion: @escaping WeatherCompletion) {
        var current: Weather?
        var forecast: WeatherForecast?
        var completionData: (success: Bool, error: NSError?) = (true, nil)
        
        let asyncGroup = DispatchGroup()
        let params = ["lat": coordinate.latitude, "lon": coordinate.longitude]
        
        // loading current weather
        asyncGroup.enter()
        RequestManager.send(path: API.Paths.current, methodType: .get, params: params) { (success, weather: Weather?, error) in
            guard success else {
                completionData.error = error
                completionData.success = false
                asyncGroup.leave()
                return
            }
            current = weather
            asyncGroup.leave()
        }
        
        // loading forecast
        asyncGroup.enter()
        RequestManager.send(path: API.Paths.forecast, methodType: .get, params: params) { (success, list: WeatherForecast?, error) in
            guard success else {
                completionData.error = error
                completionData.success = false
                asyncGroup.leave()
                return
            }
            forecast = list
            asyncGroup.leave()
        }
        
        asyncGroup.notify(queue: .main) {
            guard let current = current, let forecast = forecast else {
                completion(completionData.success, nil, completionData.error)
                return
            }
            let weatherData = WeatherDataObject(current: current, forecast: forecast)
            completion(true, weatherData, nil)
        }
    }
    
}
