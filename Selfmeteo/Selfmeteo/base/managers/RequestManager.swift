//
//  RequestManager.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation
import Alamofire

typealias Completion<T> = (_ success: Bool, _ response: T?, _ error: NSError?) -> Void where T: Decodable
typealias SuccessCompletion = (_ success: Bool, _ error: NSError?) -> Void

protocol httpRequest {
    static func send<T>(path: String, methodType: HTTPMethod, params: [String: Any]?, completion: @escaping Completion<T>) where T: Decodable
}

class RequestManager: httpRequest {
    
    static func send<T>(path: String, methodType: HTTPMethod, params: [String: Any]? = nil, completion: @escaping Completion<T>) where T: Decodable {
        guard let url = URL(string: API.serverURL + path) else {return}
        Alamofire.request(url, method: methodType, parameters: RequestManager.appendRequiredData(params), encoding: URLEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                guard let data = response.data else {
                    completion(false, nil, Errors.noInternet)
                    return
                }
                
                do {
                    let object: T = try decoder.decode(T.self, from: data)
                    let error = try decoder.decode(ErrorModel.self, from: data)
                    //try decoder.decode(SuccessModel.self, from: data) (в данном апи отсутствует success)
                    let success = error.code == ErrorModel.successCode ? true : false
                    completion(success, object, error.nsError)
                } catch {
                    completion(false, nil, Errors.parseFailure)
                }
        }
    }
    
    private static func appendRequiredData(_ params: [String: Any]?) -> [String: Any] {
        var params = params ?? [:]
        params["units"] = Defaults.metric
        params["lang"] = Defaults.localization
        params["appid"] = API.Keys.openWeatherMap
        return params
    }
    
    private static var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }
    
}


