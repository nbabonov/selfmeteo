//
//  LocationManager.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 02/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol GeolocationChangesProtocol {
    func coordinateChanged()
    func authorizationDenied()
}

class ObserveLocationManager: NSObject, CLLocationManagerDelegate {
    
    // MARK: Properties
    static let shared: ObserveLocationManager = ObserveLocationManager()
    let locationManager = CLLocationManager()
    
    var delegate: GeolocationChangesProtocol?
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var currentCoordinate: CLLocationCoordinate2D = Defaults.location
    
    // MARK: Life cycle
    override init() {
        super.init()
        if let coordinate = WeatherDataObject.getFromUD()?.current.coordinate {
            currentCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(coordinate.lat), longitude: CLLocationDegrees(coordinate.lon))
        }
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.pausesLocationUpdatesAutomatically = false
        
        // foreground
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 100
        
        // background
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    // MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        currentCoordinate = location.coordinate
        delegate?.coordinateChanged()
        if UIApplication.shared.applicationState != .active {startBackgroundTask()}
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            delegate?.authorizationDenied()
        }
        if status != .notDetermined {
            LocalNotificationManager.registerForNotifications()
        }
    }
    
    // MARK: Foreground
    func startForegroundUpdating() {
        locationManager.startUpdatingLocation()
    }
    
    func stopForegroundUpdating() {
        locationManager.stopUpdatingLocation()
    }
    
    // MARK: Background Loading Weather Task
    private func registerBackgroundTask() {
        guard backgroundTask == .invalid else {return}
        backgroundTask = UIApplication.shared.beginBackgroundTask {[weak self] in
            self?.endBackgroundTask()
        }
    }
    
    private func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }
    
    private func startBackgroundTask() {
        guard backgroundTask == .invalid else {return}
        registerBackgroundTask()
        WeatherDataManager.updateWeatherData(coordinate: currentCoordinate) {[weak self] (success, weatherData: WeatherDataObject?, error) in
            guard success, let weatherData = weatherData else {
                self?.endBackgroundTask()
                return
            }
            
            if let degree = WeatherDataObject.savedDegree(), abs(weatherData.current.details.temp - degree) > Defaults.absDegreeToNotify {
                LocalNotificationManager.showNotificationWith(weatherData)
                weatherData.saveToUD()
            } else {
                weatherData.saveToUD(needUpdateSavedDegree: false)
            }
            self?.endBackgroundTask()
        }
    }
    
}

