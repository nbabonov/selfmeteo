//
//  CurrentWeather.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct Weather: Codable {
    var city: City?
    let timeInterval: TimeInterval
    let type: [WeatherType]
    let details: WeatherDetails
    let coordinate: WeatherCoordinate?
    
    private enum CodingKeys: String, CodingKey {
        case weather
        case dt
        case main
        case city
        case coord
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        timeInterval = try TimeInterval(container.decode(Int.self, forKey: .dt))
        type = try container.decode([WeatherType].self, forKey: .weather)
        details = try container.decode(WeatherDetails.self, forKey: .main)
        coordinate = try container.decodeIfPresent(WeatherCoordinate.self, forKey: .coord)
        
        // В API город в одном роуте приходит внутри city, в другом вообще без него
        city = try? City(from: decoder)
        if city == nil {
            city = try? container.decodeIfPresent(City.self, forKey: .city)
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(city, forKey: .city)
        try container.encode(type, forKey: .weather)
        try container.encode(details, forKey: .main)
        try container.encode(timeInterval, forKey: .dt)
        try container.encode(coordinate, forKey: .coord)
    }
    
    // MARK: Helpers
    func date() -> Date {
        return Date(timeIntervalSince1970: timeInterval)
    }
    
}
