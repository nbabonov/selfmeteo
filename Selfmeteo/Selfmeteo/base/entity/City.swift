//
//  City.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct City: Codable {
    let id: Int
    var name: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        name = lozalizedName()
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
    }
    
    private func lozalizedName() -> String {
        switch name {
        case "Yekaterinburg":
            return "Екатеринбург"
        case "Chelyabinsk":
            return "Челябинск"
        case "Moscow":
            return "Москва"
        default:
            return name
        }
    }
}

