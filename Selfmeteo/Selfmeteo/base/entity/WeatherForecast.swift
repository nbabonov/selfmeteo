//
//  WeatherList.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct WeatherForecast: Codable {
    var list: [Weather]
    var days: [(date: Date, type: [WeatherType]?, minTemp: Int?, maxTemp: Int?)]
    
    private enum CodingKeys: String, CodingKey {
        case list
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        list = try container.decode([Weather].self, forKey: .list)
        
        // формируем массив данных о прогнозе погоды по дням исключая сегодняшний
        days = []
        let calendar = Calendar.current
        let daysDict = Dictionary(grouping: list, by: {calendar.startOfDay(for: $0.date())})
        for (key, value) in daysDict {
            guard Calendar.current.compare(key, to: Date(), toGranularity: .day) != .orderedSame else {continue}
            days.append((date: key, type: value.first?.type, minTemp: Int(value.sorted(by: {$0.details.tempMin < $1.details.tempMin}).first?.details.tempMin ?? 0), maxTemp: Int(value.sorted(by: {$0.details.tempMax > $1.details.tempMax}).first?.details.tempMax ?? 0)))
        }
        days.sort(by: {$0.date < $1.date})
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(list, forKey: .list)
    }
}
