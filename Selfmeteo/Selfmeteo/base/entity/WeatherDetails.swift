//
//  WeatherDetails.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct WeatherDetails: Codable {
    let temp: Float
    let tempMin: Float
    let tempMax: Float
    
    private enum CodingKeys: String, CodingKey {
        case temp
        case tempMin
        case tempMax
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(temp, forKey: .temp)
        try container.encode(tempMin, forKey: .tempMin)
        try container.encode(tempMax, forKey: .tempMax)
    }
    
}
