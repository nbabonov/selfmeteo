//
//  ErrorModel.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct ErrorModel: Decodable {

    static let successCode = 200
    
    var code: Int?
    let message: String
    
    private enum CodingKeys: String, CodingKey {
        case cod
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        // Кривое API - в одном роуте код ошибки Int, в другом String
        code = try? container.decode(Int.self, forKey: .cod)
        if code == nil {
            code = try? Int(container.decode(String.self, forKey: .cod))
        }
        
        // Также пришлось отключить парсинг сообщения, тк в одном роуте оно не приходит, а в другом оно Int 😄
        // message = try container.decode(String.self, forKey: .message)
        message = code == ErrorModel.successCode ? "" : "Ошибка OpenWeatherMap"
    }
    
    var nsError: NSError? {
        return NSError(domain: "selfmeteo", code: code ?? 0, userInfo: [NSLocalizedDescriptionKey: message])
    }
    
}

