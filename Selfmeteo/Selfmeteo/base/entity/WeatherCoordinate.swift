//
//  WeatherCoordinate.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 02/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct WeatherCoordinate: Codable {
    let lat: Float
    let lon: Float
    
    private enum CodingKeys: String, CodingKey {
        case lat
        case lon
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lat = try container.decode(Float.self, forKey: .lat)
        lon = try container.decode(Float.self, forKey: .lon)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(lat, forKey: .lat)
        try container.encode(lon, forKey: .lon)
    }
    
}
