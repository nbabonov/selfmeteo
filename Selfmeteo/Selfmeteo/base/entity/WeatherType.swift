//
//  Weather.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

struct WeatherType: Codable {
    let info: String
    let icon: String
    let iconPostfix = "@2x.png"
    
    private enum CodingKeys: String, CodingKey {
        case icon
        case description
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        info = try container.decode(String.self, forKey: .description)
        icon = try container.decode(String.self, forKey: .icon)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(info, forKey: .description)
        try container.encode(icon, forKey: .icon)
    }
    
    // MARK: Helpers
    
    func iconURL() -> URL? {
        return URL(string: API.Server.images.rawValue + icon + iconPostfix)
    }
    
}
