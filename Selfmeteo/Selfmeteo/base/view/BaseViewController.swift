//
//  BaseViewController.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 29/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
class BaseViewController: UIViewController {
    
    // MARK: Properties
    var gradientColors: (UIColor, UIColor)?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addGradientBackground()
    }
    
    // MARK: Configuring
    private func addGradientBackground() {
        guard let gradient = gradientColors else {return}
        let gradientLayer = UIColor.createGradientLayer(firstColor: gradient.0, secondColor: gradient.1)
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }

}

