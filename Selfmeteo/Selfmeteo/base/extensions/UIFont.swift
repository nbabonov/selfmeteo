//
//  UIFont.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 01/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit
import Foundation

extension UIFont {
    
    private static let SFProTextLight = "SFProText-Light"
    private static let SFProTextRegular = "SFProText-Regular"
    private static let SFProTextMedium = "SFProText-Medium"
    private static let SFProTextBold = "SFProText-Bold"
    private static let SFProTextSemibold = "SFProText-Semibold"
    
    public struct SFProText {
        static func regular(_ size: Int) -> UIFont {
            return UIFont(name: SFProTextRegular, size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
        }
        static func bold(_ size: Int) -> UIFont {
            return UIFont(name: SFProTextBold, size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
        }
        static func semibold(_ size: Int) -> UIFont {
            return UIFont(name: SFProTextSemibold, size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
        }
        static func medium(_ size: Int) -> UIFont {
            return UIFont(name: SFProTextMedium, size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
        }
        static func light(_ size: Int) -> UIFont {
            return UIFont(name: SFProTextLight, size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
        }
    }
}

