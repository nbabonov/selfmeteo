//
//  UIColor.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 30/06/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let darkMauve = UIColor(r: 69, g: 104, b: 220)
    static let lightMauve = UIColor(r: 176, g: 106, b: 179)
    
    convenience init(r: Int, g: Int, b: Int, a: CGFloat = 1) {
        self.init(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: a)
    }
    
    static func createGradientLayer(firstColor: UIColor, secondColor: UIColor) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        layer.locations = [0, 1]
        return layer
    }
    
}
