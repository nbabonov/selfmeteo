//
//  String.swift
//  Selfmeteo
//
//  Created by Nikita Babonov on 01/07/2019.
//  Copyright © 2019 Nikita Babonov. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    static func getStringWithEnding(maleBasis: String, count: Int) -> String {
        if count%10 == 1 && (count < 10 || count > 20) && count%100 != 11 {
            return maleBasis
        } else if count%10 >= 2 && count%10 <= 4 && (count < 10 || count > 20) && (count%100 != 12 && count%100 != 14 && count%100 != 13)  {
            return maleBasis + "а"
        }
        return maleBasis + "ов"
    }
}
